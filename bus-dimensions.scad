totalLength = 12 + 1 / 4;
totalWidth = 3 + 1 / 4;
sidesThickness = 1 / 4;
betweenWheelCentersLength = 6 + 1 / 4;
fromBackToDoor = 10 + 5 / 8;
fromBackToRearWheelCenter = 3 + 1 / 8;
doorThickness = 3 / 8;
doorWidth = 1 + 1 / 8;
frontLength = 1 / 2;
frontLight = 3 / 8;
frontWidth = 1 + 1 / 4;
bottomBlock = 1;
bottomBlockWidth = totalWidth - 2 * sidesThickness;
echo("bottomBlockWidth is", bottomBlockWidth);
bottomBottomLength = bottomBlock;
bottomMainLength = fromBackToDoor - bottomBlock;
bottomSecondLength = doorWidth;
bottomSideToWindow = 1 + 1 / 2;
bottomThickness = 3 / 8;
bottomThirdLength = frontLength;
bottomWidth = totalWidth - 2 * sidesThickness;
groundClearance = 3 / 8;
heightWithoutRoof = 2 + 1 / 2;
roofThickness = 3 / 8;
sidesToFloor = 1 / 8;
wheelDiameter = 1 + 1 / 2;
wheelWidth = 1 / 2;
windowHeight = 1;
rearInflection = [52 / 127, (bottomBlockWidth / 2) - (281 / 254), 0];
secondRearInflection = [52 / 127, (bottomBlockWidth / 2) + (281 / 254), 0];
bottomBlockIntersectionWithSide = [157 / 254, 0, 0];
secondBottomBlockIntersectionWithSide = [157 / 254, bottomBlockWidth, 0];
topTotalWidth = totalWidth;
topMixingIntoTheSide = [297 / 254, topTotalWidth, 0];

// Front
frontFirstPoint = [0, 0, 0];
frontSecondPoint = [frontLength, frontWidth, 0];
frontThirdPoint = [0, frontWidth * 2, 0];

// Top
topFirstPoint = [0, totalWidth / 2, 0]; // the point of the fin at the rear
topSecondPoint = secondBottomBlockIntersectionWithSide; // the point where the curb is transforming
topThirdPoint = secondRearInflection;
topFourthPoint = topMixingIntoTheSide;
topFifthPoint = [fromBackToDoor, topTotalWidth, 0];
topSixthPoint = [doorWidth + fromBackToDoor, topTotalWidth - doorThickness, 0];
topSeventhPoint = [doorWidth + fromBackToDoor + frontLength, topTotalWidth - (doorThickness + frontWidth), 0];
topEighthPoint = [doorWidth + fromBackToDoor, doorThickness, 0];
topNinethPoint = [fromBackToDoor, 0, 0];
topTenthPoint = [297 / 254, 0, 0];
topELeventhPoint = bottomBlockIntersectionWithSide;
topTwelvethPoint = rearInflection;

topPoints = [
    topFirstPoint, topSecondPoint, topThirdPoint, topFourthPoint, topFifthPoint, topSixthPoint, topSeventhPoint,
    topEighthPoint, topNinethPoint, topTenthPoint, topELeventhPoint, topTwelvethPoint
    ];

topFaces = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0
    ];

frontPoints = [
    frontFirstPoint, //0
    frontSecondPoint, //1
    frontThirdPoint, //2
    frontFirstPoint //3
    ];

frontFaces = [
        [0, 1, 2, 3]
    ];

bottomBlockPoints = [
    bottomBlockIntersectionWithSide, // 0
    rearInflection, // 1
        [0, bottomBlockWidth / 2, 0], // 2
        [bottomBlock, bottomBlockWidth / 2, 0], // 3
        [bottomBlock, 0, 0], // 4
    secondRearInflection, // 5
    secondBottomBlockIntersectionWithSide, // 6
        [bottomBlock, bottomBlockWidth, 0], // 7

    ];

bottomBlockFaces = [
        [0, 1, 2, 3, 4],
        [2, 5, 6, 7, 3]
    ];

bottomPoints = [
        [0, 0, 0], // 0 bottom
        [0, bottomWidth, 0], // 1
        [bottomMainLength, bottomWidth, 0], // 2
        [bottomMainLength + doorWidth, bottomWidth - doorThickness, 0], // 3
        [bottomMainLength + doorWidth, doorThickness, 0], // 4
        [bottomMainLength, 0, 0]/*, // 5 door

        [bottomBottomLength + bottomMainLength + bottomSecondLength, doorThickness, 0], //9
        [bottomBottomLength + bottomMainLength + bottomSecondLength, bottomWidth - doorThickness, 0], //10
        [bottomBottomLength + bottomMainLength, bottomWidth, 0], //11
        [bottomBottomLength + bottomMainLength + bottomSecondLength, doorThickness, 0], //12 front
        [bottomBottomLength + bottomMainLength + bottomSecondLength + bottomThirdLength, doorThickness + frontWidth -
        sidesThickness, 0],
    //13
        [bottomBottomLength + bottomMainLength + bottomSecondLength, bottomWidth - doorThickness, 0]*/
    ]; //11

bottomFaces = [
        [0, 1, 2, 3, 4, 5, 0]/*, // bottom
        [4, 5, 6, 7], // main
        [8, 9, 10, 11]/*, // door
        [12, 13, 14]*/
    ];

rearPoints = [
        [0, 0, 0],
        []
    ];