include <bus-dimensions.scad>
include <BOSL2/std.scad>
use <hull_polyline3d.scad>;
use <polyhedron_hull.scad>;
use <bezier_curve.scad>;
use <polyline3d.scad>;

//cube([bottomMainLength, bottomWidth, bottomThickness]);
bus();

module bus() {
    union() {
        /*bottomBlock();
        translate([bottomBlock, 0, sidesToFloor]) bottom();
        translate([fromBackToDoor + doorWidth, doorThickness - sidesThickness, 0])front();*/
        /*translate([0,0,heightWithoutRoof]) */top();
    }
}

module top() {
    color("blue") linear_extrude(height = heightWithoutRoof) projection() color("blue") polyhedron(points =
    topPoints, faces = topFaces);
}

module front() {
    color("blue") linear_extrude(height = heightWithoutRoof) projection() color("blue") polyhedron(points =
    frontPoints, faces = frontFaces);
}

module bottom() {
    color("red") linear_extrude(height = bottomThickness) projection() color("blue") polyhedron(points = bottomPoints,
    faces = bottomFaces);
}

module bottomBlock() {
    difference() {
        color("yellow") union() {
            color("blue") linear_extrude(height = heightWithoutRoof) projection() color("blue") polyhedron(points =
            bottomBlockPoints, faces = bottomBlockFaces);

            t_step = 0.05;
            width = 1 / 4;

            p0 = bottomBlockPoints[0];
            p1 = bottomBlockPoints[1];
            p2 = bottomBlockPoints[2];
            p3 = bottomBlockPoints[3];
            p4 = bottomBlockPoints[5];
            p5 = bottomBlockPoints[6];
            bezierCurvePoints = bezier_curve(t_step, [p0, p1, p2]);
            /*,bezier_curve(t_step, [p3, p4, p5]));*/
            echo("Curve", bezierCurvePoints);
            points = concat(bezierCurvePoints);
            /*faces = concat([for (i = [0 : 1 : len(points) - 1]) i], 0);*/
            echo("Number of points of the Bezier curve: ", len(points));
            /*color("yellow") translate([width/3, 0, 0]) linear_extrude(height = heightWithoutRoof) projection() polyline3d(
            points = points, diameter = width, $fn = 24);*/
            translate([0.1, 0, 0]) union() {
                color("purple") linear_extrude(height = heightWithoutRoof) projection() translate([- 0.1, 0, 0])
                    hull_polyline3d(bezierCurvePoints, diameter = 0.1, $fn = 24);

                secondBezierCurvePoints = bezier_curve(t_step, [p2, p4, p5]);
                color("purple") linear_extrude(height = heightWithoutRoof) projection() translate([- 0.1, 0, 0])
                    hull_polyline3d(secondBezierCurvePoints, diameter = 0.1, $fn = 24);
            }
            /*hull_polyline3d(points, diameter = 0.1, $fn = 24);*/
            //polyhedron_hull(points);


        }
        translate([0, - bottomBlockWidth, 0]) cube([bottomBlock, bottomBlockWidth, heightWithoutRoof]);
        translate([0, bottomBlockWidth, 0]) cube([bottomBlock, bottomBlockWidth, heightWithoutRoof]);
    }
}

//